{ config, ... }:

{
  # virtualbox
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "tim" ];
  # this does not seem to automatically install GA
  virtualisation.virtualbox.guest.enable = true;

  # Docker
  virtualisation.docker.enable = true;
}
