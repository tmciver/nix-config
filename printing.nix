{ config, ... }:

{
  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable auto-discovery of network printers
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };
}
