{ config, ... }:

{
  fileSystems."/mnt/mciver-share" = {
      device = "//raspberrypi/McIver Share";
      fsType = "cifs";
      options = let
        # this line prevents hanging on network split
        automount_opts = "file_mode=0755,dir_mode=0755,uid=1000,gid=100";

      in ["${automount_opts},credentials=/etc/nixos/smb-secrets"];
  };
}
