{ config, pkgs, ... }:

{

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

  # Set your time zone.
  time.timeZone = "America/New_York";

  # allow licensed applications like Slack
  nixpkgs.config.allowUnfree = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable the FTP daemon.
  services.vsftpd = {
    enable = true;
    localUsers = true;
    writeEnable = true;
    localRoot = "/var/ftp";
    extraConfig = ''
      local_umask=007
      pasv_enable=Yes
      pasv_min_port=51000
      pasv_max_port=51999
    '';
  };

  services.udev.packages = [ pkgs.android-udev-rules ];

  # rainbow-hash systemd services
  systemd.services =
    let
      rhSrc = pkgs.fetchFromGitHub {
        owner = "tmciver";
        repo = "rainbow-hash";
        rev = "1abedaa6a0f9a16a27755ba5ecdd394b43413d6a";
        hash = "sha256-ecy3hPyer51k5FYapab5i3OTnfw8/QB51uKct4gbovE=";
      };
      rh = pkgs.haskellPackages.callCabal2nix "rainbow-hash" rhSrc {};
      storage-dir = "/mnt/mciver-share/app/rainbow-hash/data";
      watch-dir = "/var/ftp";
    in {
      rh-server = {
        description = "A hash-based blob storage web service";
        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          ExecStart = "${rh}/bin/rh-server --storage-dir ${storage-dir}";
        };
      };

      rh-cli = {
        description = "CLI client for rainbow-hash blob storage service";
        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          ExecStart = "${rh}/bin/rh-cli watch ${watch-dir} --delete-after-upload";
        };
      };
    };

  # Enable Bluetooth
  hardware.bluetooth.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget git emacs tree thunderbird element-desktop firefox libreoffice perkeep
    docker-compose zip unzip okular openssl pidgin gnumake coreutils file
    dmenu direnv inetutils android-tools
  ];

}
