{ config, ... }:

{
  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 21 22 80 ];
  networking.firewall.allowedTCPPortRanges = [ { from = 51000; to = 51999; } ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
}
