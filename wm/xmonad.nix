{ config, lib, pkgs, ... }:

{
  services = {
    gnome.gnome-keyring.enable = true;
    upower.enable = true;

    dbus = {
      enable = true;
      packages = [ pkgs.dconf ];
    };

    libinput = {
      enable = true;
      touchpad.disableWhileTyping = true;
      touchpad.naturalScrolling = true;
    };

    displayManager = {
      sddm.enable = true;
      defaultSession = "none+xmonad";
    };

    xserver = {
      enable = true;
      xkb.layout = "us";

      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
      };

#      xkbOptions = "caps:ctrl_modifier";
    };
  };

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  systemd.services.upower.enable = true;
}
