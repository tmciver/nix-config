{ config, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = false;
  users.users.tim = {
    isNormalUser = true;
    home = "/home/tim";
    description = "Tim McIver";
    extraGroups = [ "wheel" "networkmanager" "docker" "vsftpd" ]; # Enable ‘sudo’ for the user.
    hashedPassword = "$6$z5TjlS9wSvz$dMsUqSyr4E1PVs4sm1LgHKeLRDyrtBCXCLsNict632MhvIJNUuyMaf8ZI8NTpbnupPortL5y/aMHk1HPTsDJ1/";
  };
}
